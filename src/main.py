from functools import wraps

def my_input(log_file):
    def my_decorator(func):
        @wraps(func)
        def logging(*args,**kwargs):
            ''' Logs calls to my_function.log'''
            with open(log_file, 'a') as log:
                log.write (f"1. Running function: {func.__name__}\n")
                res = func(*args, **kwargs)
                log.write (f"2. Done running function: {func.__name__}\n")
                return res
        return logging
    return my_decorator
    

@my_input("my_function.log")
def calculate(x,y):
    '''Adds two numbers'''
    return x+y

function = calculate(5,5)
print(f"This is from function {function}")
